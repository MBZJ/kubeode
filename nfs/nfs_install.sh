#!/bin/bash
#单机版

IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."$3}')) ||IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."}'))

_single  ()  {
 yum install -y nfs-utils rpcbind
mkdir -pv  /data/nfs/k8s
chown -R nfsnobody.nfsnobody /data/nfs/k8s
echo  '/data/nfs/k8s  *(rw,async,no_root_squash)'   >> /etc/exports
 systemctl restart rpcbind
  systemctl enable rpcbind
 systemctl enable nfs
 systemctl restart nfs
}
_single

_master_nfs  ()  {
cd /root/K8s/nfs/; helm install  nfs-client  -n kube-system --set nfs.server=${IP} --set "nfs.path=/data/nfs/k8s  ,storageClass.name=nfs"  ./nfs-client-provisioner/
}
_master_nfs

 kubectl patch storageclass  nfs  -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'  